import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomePageRoutingModule} from './home-page-routing.module';
import {ListComponent} from './list/list.component';
import {FiltersComponent} from './filters/filters.component';
import {CalendarModule} from 'primeng/calendar';
import {InputTextModule} from 'primeng/inputtext';
import {MultiSelectModule} from 'primeng/multiselect';
import {HomePageComponent} from './home-page.component';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {HttpClientModule} from '@angular/common/http';
import {RadioButtonModule} from 'primeng/radiobutton';


@NgModule({
  declarations: [
    ListComponent,
    FiltersComponent,
    HomePageComponent
  ],
  exports: [
    FiltersComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    HomePageRoutingModule,
    CalendarModule,
    InputTextModule,
    MultiSelectModule,
    FormsModule,
    DropdownModule,
    HttpClientModule,
    RadioButtonModule
  ]
})
export class HomePageModule {
}

