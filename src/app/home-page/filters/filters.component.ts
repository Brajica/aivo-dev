import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import FiltersParams from '../models/filtersParams';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  @Output() eventFilter: EventEmitter<any> = new EventEmitter<any>();
  @Input() filterParams: FiltersParams;
  @Input() listOrderBy;
  @Input() listOptions;
  @Input() listYears;
  constructor() { }

  ngOnInit(): void {
  }

}
