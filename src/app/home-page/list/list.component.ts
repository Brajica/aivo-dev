import {Component, Input, OnInit} from '@angular/core';
import {Movies} from '../models/movies';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() movies: Array<Movies>;
  constructor() { }

  ngOnInit(): void {
  }

}
