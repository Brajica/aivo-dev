import {ProgramType} from './movies';

export default interface FiltersParams {
  date?: string;
  name?: string;
  orderBy?: string;
  typeOrder?: orderBy;
  typeProgram?: ProgramType;
}

export enum orderBy {
  'DESC' = 'DESC',
  'ASC' = 'ASC'
}

export const listOrderBy = [
  {name: 'Ascendete', value: orderBy.ASC},
  {name: 'Descendente', value: orderBy.DESC}
];
