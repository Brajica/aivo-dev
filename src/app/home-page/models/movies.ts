export interface Movies {
  'title': string;
  'description': string;
  'programType': ProgramType;
  'images': {
    'Poster Art': {
      'url': string;
      'width': any;
      'height': any;
    }
  };
  'releaseYear': any;

}

export enum ProgramType {
  'series' = 'series',
  'movie' = 'movie'
}

export const listOption = [
  {value: ProgramType.series, name: 'Series'},
  {value: ProgramType.movie, name: 'Peliculas'},
];
