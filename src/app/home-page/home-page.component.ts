import {Component, OnInit} from '@angular/core';
import FiltersParams, {listOrderBy, orderBy} from './models/filtersParams';
import {HomePageService} from './services/home-page.service';
import {listOption, Movies} from './models/movies';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  filterParams: FiltersParams;
  listOrderBy = listOrderBy;
  lisOptions = listOption;
  listYears: Array<any> = [];
  movies: Array<Movies> = [];

  constructor(private homePageService: HomePageService) {
  }

  async ngOnInit() {
    this.initFilters();
    this.getMovies();
    this.listYears = this.homePageService.getYearstoFilter({fromYear: 1945, toYear: 2021});
  }

  initFilters() {
    this.filterParams = {
      date: null,
      name: null,
      orderBy: 'title',
      typeOrder: orderBy.ASC
    };
  }

  async getMovies() {
    try {
      this.movies = await this.homePageService.getMovies(this.filterParams).toPromise();
    }catch (e) {

    }
  }

}

