import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Movies} from '../models/movies';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import FiltersParams, {orderBy} from '../models/filtersParams';

@Injectable({
  providedIn: 'root'
})
export class HomePageService {
  private movies: Array<Movies> = [];

  constructor(private httpClient: HttpClient) {
  }


  getYearstoFilter({fromYear, toYear}: { fromYear: number, toYear: number }) {
    const listYears: Array<any> = [];
    for (let i = fromYear; i <= toYear; i++) {
      listYears.push({name: i.toString(), value: i.toString()});
    }
    return listYears;
  }


  getMovies(paramsFilter?: FiltersParams): Observable<Array<Movies>> {

    if (this.movies.length > 0) {
      return new Observable(obs => {
        obs.next(this.filterMovies(paramsFilter));
        obs.complete();
      });
    } else {
      return this.requestMovies(paramsFilter).pipe();
    }

  }

  private requestMovies(paramsFilter?: FiltersParams): Observable<Array<Movies>> {
    return this.httpClient.get(`${environment.api_url}movies`).pipe(
      map((res: any) => {
        this.movies = res.entries;
        return this.filterMovies(paramsFilter);
      })
    );
  }

  private filterMovies(paramsFilter: FiltersParams): Movies[] {
    let filterMovies = paramsFilter.name ? this.movies.filter(itemc => itemc.title.toLowerCase().includes(paramsFilter.name.toLowerCase())) : this.movies;
    filterMovies = paramsFilter.date ? this.movies.filter(item => item.releaseYear.toString() == paramsFilter.date) : filterMovies;
    filterMovies = paramsFilter?.typeProgram ? this.movies.filter(item => item.programType.toString() == paramsFilter?.typeProgram) : filterMovies;

    if (paramsFilter.typeOrder == orderBy.ASC) {
      filterMovies.sort((a, b) => a[paramsFilter.orderBy] > b[paramsFilter.orderBy] ? 1 : -1);
    } else if (paramsFilter.typeOrder == orderBy.DESC) {
      filterMovies.sort((a, b) => a[paramsFilter.orderBy] > b[paramsFilter.orderBy] ? 1 : -1).reverse();
    }

    return filterMovies;
  }


}
