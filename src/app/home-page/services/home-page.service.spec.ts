import { TestBed } from '@angular/core/testing';

import { HomePageService } from './home-page.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('HomePageService', () => {
  let service: HomePageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(HomePageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
