import { TestBed } from '@angular/core/testing';

import { HomePageGuard } from './home-page.guard';
import {RouterTestingModule} from '@angular/router/testing';

describe('HomePageGuard', () => {
  let guard: HomePageGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    guard = TestBed.inject(HomePageGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
