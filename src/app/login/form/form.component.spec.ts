import {ComponentFixture, TestBed} from '@angular/core/testing';

import {FormComponent} from './form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginService} from '../services/login.service';
import {CardModule} from 'primeng/card';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {LoginComponent} from '../login.component';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormComponent, LoginComponent],
      imports: [InputTextModule, CardModule, ButtonModule, ReactiveFormsModule],
      providers: [LoginService]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('When component is initilizated', () => {
    it('Should create the forms', () => {
      expect(Object.keys(component.form.controls)).toEqual(['email', 'password']);
    });
  });
});
