import {Component, OnInit} from '@angular/core';
import {LoginService} from './services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  request: any = {
    error: false,
    loading: false
  };

  constructor(private Ls: LoginService, private router: Router) {
  }

  ngOnInit(): void {
  }

  async requestLogin(body) {
    this.request.loading = true;

    try {
      const response = await this.Ls.login(body).toPromise();
      localStorage.setItem('avio-dev', response.accessToken);
      this.router.navigate(['home']);
      this.request.loading = false;
    } catch (e) {
      this.request = {
        loading: false,
        error: true
      };

    }

  }

}
