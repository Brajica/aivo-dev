import {TestBed} from '@angular/core/testing';

import {LoginService} from './login.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../environments/environment';

describe('LoginService', () => {
  let service: LoginService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  let dataError;
  let dataResponse;
  let bodyRequest = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(LoginService);
  });

  afterEach(() => {
    dataError = undefined;
    dataResponse = undefined;
    bodyRequest = {};
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Test to login request', () => {
    it('should return object ', () => {
      // arrange - preparar
      const expectData = {
        accesToken: 'dfsfdsfds',
        info: {
          id: 1
        }
      };

      bodyRequest = {
        email: 'jime@gmail.com',
        paswword: 'dfdsfsd'
      };

      // act - actuar
      service.login(bodyRequest).subscribe(
        response => dataResponse = response,
        error => dataError = error
      );
      const req = httpTestingController.expectOne(
        `${environment.api_url}login`
      );
      req.flush(expectData);

      /*assert */
      expect(!dataResponse.error).toBeTrue();
      expect(req.request.method).toEqual('POST');
      expect(dataError).toBeUndefined();
    });

    it('should return [key] error ', () => {
      // arrange - preparar
      const expectData: ErrorEvent | any = {
        error: true
      };
      // act - actuar
      service.login(bodyRequest).subscribe(
        response => dataResponse = response,
        error => dataError = error
      );
      const req = httpTestingController.expectOne(
        `${environment.api_url}login`
      );
      req.error(expectData);

      /*assert */

      expect(dataError.error).toBeDefined();
      expect(req.request.method).toEqual('POST');
      expect(dataResponse).toBeUndefined();
    });
  });
});
