import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginModule} from './login/login.module';
import {HomePageGuard} from './home-page/home-page.guard';
import {LoginGuard} from './login/login.guard';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule), canActivate:  [LoginGuard]},
  {path: 'home', loadChildren: () => import('./home-page/home-page.module').then(m => m.HomePageModule), canActivate: [HomePageGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
